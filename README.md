# StreamLoots Challenge by Carlos Lopez
First, im use my own scaffolding based in my github: https://github.com/foxkdev/ddd-node-scaffold

Im use DDD to design my API, and im separate layers in application,infra,domain.

Im have Unit Tests to domain, use case and infrastructure, but not in all use cases, im havent more time to implement in all, and im prefer 
develop features, but when im use tests im prefer use TDD if is possible with the feature.

Im use Mongo Atlas to database, im always use this to my projects if its nosql.

In my scaffold i have implementation to sql, but I have anothers projects implemented mongoose and im implemented too in this challenge, 
its more easy to me use ORM vs native mongo querys, but i can use native too, in current job we havent ORM with mongo.

All my projects are in Docker.

#Features
Im develop 4 endpoints:
- Get Cards
- Update card
- Publish in bulk
- Unpublish un bulk

Im develop events to:
- Card created
- Card published

Im separate this implementation in infrastructure service because its more easy change to Pub/Sub, Kafka, RabbitMQ or other implementation.

Im develop event submitters with 2 implementation to simulate analytics sources.

Another option is not use event sourcing and implement directly in infrastructure service layer to send metrics to two providers, but im prefer show you event sourcing in this challenge.
I think in real use case its better have another API to manage this metrics, same stats, send event and other API subscribe to this event and have stats or other feature.

About authorization im only decode JWT with middleware, but if im have real case to production im validate jwt to know if token is valid or not and get userId and other features.


About performance of API with this numbers(200k in 2 min), we can have 2 problems about scalability:

- APIS scalability(pods in docker) -> its more easy to resolve, increment pods in k8 and memory/cpu limits
- MongoDB -> its more easy too, increment size in Mongo Atlas.

But im prefer before increment pods, or mongo, develop performance test, im suggest use k6.io, is a library in js open source to have performance tests more easy to implement.
With more time i can develop this tests, in current job we use this every critical feature, same sigin/singup etc.

And im know this api can with 200k in 2 min with correctly resources.

Not for this features, but for example in signin/singup process if you want increment performance for example im suggest use redis to have cache in tokens and more.

If you have problems with performance(memory leaks or others tipical problems) im suggest use APM, same new relic, datadog or elastic APM, to know your real timing when have real requests.

#Next Features to develop
Im suggest create task in next sprint(2 weeks) to:
- Implement new API with stats and create events when card is used and cards obtaineds.
- Implement new API to only manage metrics, im know have 100k/req by min have more custom metrics to analytics platform, so i suggest create api to manage this analytics platforms and create endpoint or listen events to send to 1,2 or all platform metrics.
- I havent any endpoint to create card, Im create manualy in database, im know in production we have UI to user create card, etc, so, we need this endpoint.
- Im suggest try to have published cards in redis and compare performace in getCards vs storaged in Mongo.


#Resources

Im add collection of postman in repo to test it, you only need import this.

Mongo is accessed for all users with credentials, is public, you have credentials in .env file



