require('dotenv').config()
const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const router = require('./router')
const container = require('./container')
class Server {
  constructor() {
    this.app = express()
    this.app.use(cors())
    this.app.use(express.json()) // for parsing application/json
    this.app.use(express.urlencoded({extended: true})) // for parsing application/x-www-form-urlencoded
    this.app.use(morgan(':method :url :status :response-time ms'))

    this.app.use(router)
  }
  start() {
    return new Promise((resolve, reject) => {
      const port = process.env.NODE_PORT || 3020
      try {
        this.server = this.app.listen(port, () => {
          console.log(`🤘 API - Port ${port}`)
        })
        const subscriberPublishCard = container.resolve('subscriberPublishCard')
        subscriberPublishCard.subscribe()

        const subscriberCreatedCard = container.resolve('subscriberCreatedCard')
        subscriberCreatedCard.subscribe()

        resolve(this.server)
      } catch (err) {
        reject(err)
      }
    })
  }
}

const server = new Server()
server.start().catch((error) => {
  console.error(error.stack)
  process.exit()
})
module.exports = {app: server.app, server: server.server}
