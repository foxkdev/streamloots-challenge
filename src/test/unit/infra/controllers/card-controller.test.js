const {app, server} = require('../../../../server')
const supertest = require('supertest')
const request = supertest(app)
const awilix = require('awilix')
const container = require('../../../../container')
const {OK, NOT_AUTHORIZED, BAD_REQUEST} = require('../../../../infra/http-status-code')
describe('card controller', () => {
  // eslint-disable-next-line max-len
  const tokenOwner = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdHJlYW1sb290cy5jb20iLCJ1c2VySWQiOiI1YTUwMTU5MzA4ZjVhODAwMTExZGU3NTkiLCJpYXQiOjE1MTYyMzkwMjJ9.mj8-t--lfImQGg8HoA_9XOvDlunl3YJoPttkIbOHNMU'
  const ownerId = '5a50159308f5a800111de759'

  afterEach(async () => {
    await server.close()
  })
  describe('GET cards', () => {
    test('should return 400  when calling without Authorization bearer', async () => {
      const expectedErrors = {
        errors: [
          {
            message: `Field 'authorization' cannot be blank`,
            description: 'invalid params',
          },
        ],
      }
      const res = await request.get('/api/v1/cards')
      const {status, body} = res
      expect(status).toBe(BAD_REQUEST)
      expect(body).toEqual(expectedErrors)
    })

    test('should return 401 when calling with Authorization bearer invalid', async () => {
      const res = await request.get('/api/v1/cards').set({Authorization: `Bearer`})
      const {status} = res
      expect(status).toBe(NOT_AUTHORIZED)
    })
    test('should return 200 when calling with Owner Token', async () => {
      const getCardsMock = {
        get: jest.fn(),
      }
      getCardsMock.get.mockResolvedValue([])

      container.register({
        getCards: awilix.asValue(getCardsMock),
      })
      const res = await request.get('/api/v1/cards').set({Authorization: `Bearer ${tokenOwner}`})
      const {status, body, headers} = res
      expect(getCardsMock.get).toHaveBeenCalledTimes(1)
      expect(getCardsMock.get).toHaveBeenCalledWith({userId: ownerId})
      expect(status).toBe(OK)
      expect(body).toEqual([])
      expect(headers['content-type']).toContain('application/json')
    })
  })
})
