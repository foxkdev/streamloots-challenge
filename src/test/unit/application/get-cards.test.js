const {PUBLISHED} = require('../../../domain/card/card-status')
const {REGULAR} = require('../../../domain/card/card-types')
const awilix = require('awilix')
const container = require('../../../container')
const getCardsCommandBuilder = require('../../../application/get_cards/get-cards-command')
const cardId = 'card1'
const ownerId = '5a50159308f5a800111de759'
const userId = '5a50159308f5a800111de750'
describe('get cards use case', () => {
  let cardRepositoryMock
  let cardsStatsServiceMock
  let getCards
  beforeEach(() => {
    cardRepositoryMock = {
      getCards: jest.fn(),
    }
    cardsStatsServiceMock = {
      getOwnerStats: jest.fn(),
      getUserStats: jest.fn(),
    }
    container.register({
      cardRepository: awilix.asValue(cardRepositoryMock),
      cardsStatsService: awilix.asValue(cardsStatsServiceMock),
    })
    getCards = container.resolve('getCards')
  })
  afterEach(() => jest.clearAllMocks())

  test('should return cards with stats only card with owner id', async () => {
    const cardsRepositoryMockExpected = [
      {id: cardId, name: null, image: null, rarity: null, status: PUBLISHED, cardType: {type: REGULAR, availableCards: null}, ownerId},
      {id: cardId, name: null, image: null, rarity: null, status: PUBLISHED, cardType: {type: REGULAR, availableCards: null}, ownerId: userId},
    ]
    cardRepositoryMock.getCards.mockReturnValue(cardsRepositoryMockExpected)
    cardsStatsServiceMock.getOwnerStats.mockReturnValue({totalObtainedCards: 10, totalUsedCardsByAllUsers: 100})
    cardsStatsServiceMock.getUserStats.mockReturnValue({totalObtainedCards: 10, totalUsedCards: 5})

    const getCardsResponseExpected = {
      cards: [
        {
          id: cardId,
          name: null,
          image: null,
          rarity: null,
          status: PUBLISHED,
          type: {type: REGULAR, availableCards: null},
          ownerId,
          stats: {
            totalObtainedCards: 10,
            totalUsedCardsByAllUsers: 100,
          },
        },
        {
          id: cardId,
          name: null,
          image: null,
          rarity: null,
          status: PUBLISHED,
          type: {type: REGULAR, availableCards: null},
          ownerId: userId,
          stats: {totalObtainedCards: 10,
            totalUsedCards: 5,
          },
        },
      ],
    }
    const getCardsCommand = getCardsCommandBuilder({userId: ownerId})
    const getCardsResponse = await getCards.get(getCardsCommand)
    expect(getCardsResponse).toEqual(getCardsResponseExpected)
    expect(cardRepositoryMock.getCards).toHaveBeenCalledTimes(1)
    expect(cardsStatsServiceMock.getOwnerStats).toHaveBeenCalledTimes(1)
    expect(cardsStatsServiceMock.getUserStats).toHaveBeenCalledTimes(1)
  })
})
