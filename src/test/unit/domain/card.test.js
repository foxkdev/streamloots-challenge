const cardBuilder = require('../../../domain/card/card')
const CardStatusInvalidError = require('../../../domain/card/errors/card-status-invalid-error')
const CardTypeInvalidError = require('../../../domain/card/errors/card-type-invalid-error')
const FieldEmptyError = require('../../../domain/errors/field-empty-error')
const {PUBLISHED, HIDDEN} = require('../../../domain/card/card-status')
const {REGULAR, LIMITED} = require('../../../domain/card/card-types')
const CardCreatedEvent = require('../../../domain/card/events/card-created-event')
const CardPublishedEvent = require('../../../domain/card/events/card-published-event')
describe('card domain', () => {
  const cardId = 'card1'
  const ownerId = 'ownerID'
  test('should throw exception without id', async () => {
    expect(() => {
      cardBuilder.build({})
    }).toThrow(FieldEmptyError)
  })
  test('should throw exception with invalid status', async () => {
    expect(() => {
      cardBuilder.build({id: cardId, status: 'invalid'})
    }).toThrow(CardStatusInvalidError)
  })
  test('should throw exception with invalid type', async () => {
    expect(() => {
      cardBuilder.build({id: cardId, status: PUBLISHED, cardType: ''})
    }).toThrow(CardTypeInvalidError)
  })
  test('should throw exception with invalid ownerId', async () => {
    expect(() => {
      const card = cardBuilder.build({id: cardId, status: PUBLISHED, cardType: {type: REGULAR, availableCards: null}})
      card.type.availableCards = 10
    }).toThrowError('field ownerId empty')
  })
  test('should throw exception when set avaiableCards in card with type regular', async () => {
    expect(() => {
      const card = cardBuilder.build({id: cardId, status: PUBLISHED, cardType: {type: REGULAR, availableCards: null}, ownerId: ownerId})
      card.cardType.availableCards = 10
    }).toThrowError('field availableCard only can set in type limited')
  })
  test('should return valid card with type regular', async () => {
    const card = cardBuilder.build({id: cardId, status: PUBLISHED, cardType: {type: REGULAR, availableCards: null}, ownerId})
    expect(card.id).toBe(cardId)
    expect(card.status).toBe(PUBLISHED)
    expect(card.cardType.type).toBe(REGULAR)
    expect(card.ownerId).toBe(ownerId)
    expect(card.getEvents().length).toBe(1)
    expect(card.getEvents()[0]).toBeInstanceOf(CardCreatedEvent)
  })
  test('should return valid card with type limited', async () => {
    const card = cardBuilder.build({id: cardId, status: PUBLISHED, cardType: {type: LIMITED, availableCards: null}, ownerId})
    card.cardType.availableCards = 10
    expect(card.id).toBe(cardId)
    expect(card.status).toBe(PUBLISHED)
    expect(card.cardType.type).toBe(LIMITED)
    expect(card.ownerId).toBe(ownerId)
    expect(card.getAvailableCards()).toBe(10)
    expect(card.getEvents().length).toBe(1)
    expect(card.getEvents()[0]).toBeInstanceOf(CardCreatedEvent)
  })
  test('should throw exception when publish and cannot set name, image or rarity', async () => {
    expect(() => {
      const card = cardBuilder.build({
        id: cardId,
        status: HIDDEN,
        cardType: {type: LIMITED, availableCards: null},
        ownerId,
      })
      card.publish()
    }).toThrowError('card require fields name, image and rarity to publish')
  })
  test('should return valid card when publish', async () => {
    const card = cardBuilder.build({
      id: cardId,
      name: 'card1',
      image: 'image1',
      rarity: 'rarity1',
      status: HIDDEN,
      cardType: {type: LIMITED, availableCards: 10},
      ownerId,
    })
    card.publish()
    expect(card.id).toBe(cardId)
    expect(card.status).toBe(PUBLISHED)
    expect(card.cardType.type).toBe(LIMITED)
    expect(card.ownerId).toBe(ownerId)
    expect(card.getEvents().length).toBe(2)
    expect(card.getEvents()[0]).toBeInstanceOf(CardCreatedEvent)
    expect(card.getEvents()[1]).toBeInstanceOf(CardPublishedEvent)
  })
})
