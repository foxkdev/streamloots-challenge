const env = process.env.NODE_ENV || 'run'


const run = {
  mongo: {
    uri: process.env.MONGO_URI,
  },
}

const test = {
  mongo: {
    uri: process.env.MONGO_URI || 'mongodb://mongo@mongo',
  },
}
const config = {
  run,
  test,
}
module.exports = config[env]
