const awilix = require('awilix')
const {createContainer, asClass, asFunction, asValue, InjectionMode} = awilix
const config = require('./config')
const EventBus = require('./infra/services/event-bus')
const db = require('./infra/persistence/mongo')

const getCards = require('./application/get_cards')
const updateCard = require('./application/update_card')
const changeStatusCard = require('./application/change_status_card')
const cardsStatsService = require('./infra/services/cards-stats')
const subscriberPublishCard = require('./infra/services/subscriber-publish-card')
const subscriberCreatedCard = require('./infra/services/subscriber-created-card')

const cardRepository = require('./infra/persistence/mongo/repositories/card-repository')

const container = createContainer({
  injectionMode: InjectionMode.PROXY,
})
// global
container.register({
  config: asValue(config),
  eventBus: asClass(EventBus).singleton(),
  subscriberPublishCard: asClass(subscriberPublishCard),
  subscriberCreatedCard: asClass(subscriberCreatedCard),
})
// repositories
container.register({
  db: asFunction(db).singleton(),
  cardRepository: asClass(cardRepository),
})
// applications
container.register({
  getCards: asClass(getCards),
  cardsStatsService: asClass(cardsStatsService),
  updateCard: asClass(updateCard),
  changeStatusCard: asClass(changeStatusCard),
})
module.exports = container
