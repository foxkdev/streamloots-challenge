const FieldEmptyError = require('./errors/field-empty-error')

class Domain {
  constructor({requireds = null}) {
    if (this.constructor === Domain) {
      throw new TypeError('Abstract class Domain cannot be instantiated directly')
    }
    this.requireds = requireds
    this.events = []
    this.checkRequired()
  }
  checkRequired() {
    if (!this.requireds || this.requireds.length > 0) {
      this.requireds.forEach((prop) => {
        Object.defineProperty(this, prop, {
          set: (newVal) => {
            if (!newVal) {
              throw new FieldEmptyError(`field ${prop} empty`)
            }
            this[`_${prop}`] = newVal
          },
          get: () => {
            return this[`_${prop}`]
          },
        })
      })
    }
  }
  addEvent(event) {
    this.events.push(event)
  }
  getEvents() {
    return this.events
  }
  clearEvents() {
    this.events = []
  }
}


module.exports = Domain
