const DomainEvent = require('../../domain-event')
class CardCreatedEvent extends DomainEvent {
  constructor({cardId, card}) {
    super({name: 'card-created', domain: card.toObject()})
    this.payload = {id: cardId}
  }
}

module.exports = CardCreatedEvent
