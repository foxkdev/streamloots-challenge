const DomainEvent = require('../../domain-event')
class CardPublishedEvent extends DomainEvent {
  constructor({cardId, card}) {
    super({name: 'card-published', domain: card.toObject()})
    this.payload = {id: cardId}
  }
}

module.exports = CardPublishedEvent
