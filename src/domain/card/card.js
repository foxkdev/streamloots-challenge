const Domain = require('../domain')
const cardStatus = require('./card-status')

const CardStatusInvalidError = require('./errors/card-status-invalid-error')
const CardEmptyFieldsError = require('./errors/card-empty-fields-error')
const CardCreatedEvent = require('./events/card-created-event')
const CardPublishedEvent = require('./events/card-published-event')
const {isEmpty} = require('../../infra/helpers')
const CardType = require('./card-type')

class Card extends Domain {
  constructor({id, name, image, rarity, status, cardType, ownerId, createdAt, updatedAt}) {
    super({requireds: ['id', 'ownerId']})
    this.id = id
    this.name = name || null
    this.image = image || null
    this.rarity = rarity || null
    this.status = status
    this.cardType = cardType
    this.ownerId = ownerId
    this.createdAt = createdAt
    this.updatedAt = updatedAt
  }
  static build({id, name, image, rarity, status, cardType, ownerId, createdAt, updatedAt}) {
    const card = new Card({id, name, image, rarity, status, cardType, ownerId, createdAt, updatedAt})
    return card
  }
  set status(status) {
    if (!Object.values(cardStatus).includes(status)) {
      throw new CardStatusInvalidError('card status invalid', this.id)
    }
    this._status = status
  }
  get status() {
    return this._status
  }
  set cardType(type) {
    this._cardType = new CardType({type: type.type, availableCards: type.availableCards})
  }
  get cardType() {
    return this._cardType
  }
  set createdAt(createdAt) {
    this._createdAt = createdAt
    this.addEvent(new CardCreatedEvent({cardId: this.id, card: this}))
  }
  get createdAt() {
    return this._createdAt
  }
  set updatedAt(updatedAt) {
    this._updatedAt = updatedAt
  }
  get updatedAt() {
    return this._updatedAt
  }
  publish() {
    if (isEmpty(this.name) || isEmpty(this.image) || isEmpty(this.rarity) || isEmpty(this.cardType.availableCards)) {
      throw new CardEmptyFieldsError('card require fields name, image and rarity to publish', this.id)
    }
    this.status = cardStatus.PUBLISHED
    this.addEvent(new CardPublishedEvent({cardId: this.id, card: this}))
  }
  unpublish() {
    this.status = cardStatus.HIDDEN
  }
  getAvailableCards() {
    return this.cardType.availableCards
  }
  toObject() {
    return {
      id: this.id,
      name: this.name,
      image: this.image,
      rarity: this.rarity,
      status: this.status,
      type: this.cardType?.type,
      ownerId: this.ownerId,
    }
  }
}
module.exports = Card
