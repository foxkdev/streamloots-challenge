const cardTypes = require('./card-types')
const CardTypeInvalidError = require('./errors/card-type-invalid-error')
class CardType {
  constructor({type, availableCards = null}) {
    this.type = type
    this.availableCards = availableCards
  }
  set type(type) {
    if (!Object.values(cardTypes).includes(type)) {
      throw new CardTypeInvalidError('card type invalid')
    }
    this._type = type
  }
  get type() {
    return this._type
  }
  set availableCards(availableCards) {
    if (availableCards && this.type === cardTypes.REGULAR) {
      throw new CardTypeInvalidError('field availableCard only can set in type limited')
    }
    this._availableCards = availableCards
  }
  get availableCards() {
    return this._availableCards
  }
}
module.exports = CardType
