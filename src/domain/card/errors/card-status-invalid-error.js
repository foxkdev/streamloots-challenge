const ApplicationError = require('../../application-error')

class CardStatusInvalidError extends ApplicationError {
  constructor(message, cardId) {
    super(message)
    this.name = 'CardStatusInvalidError'
    this.cardId = cardId
  }
}

module.exports = CardStatusInvalidError
