const ApplicationError = require('../../application-error')

class CardEmptyFieldsError extends ApplicationError {
  constructor(message, cardId = null) {
    super(message)
    this.name = 'CardEmptyFieldsError'
    this.cardId = cardId
  }
}

module.exports = CardEmptyFieldsError
