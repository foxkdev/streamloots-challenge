const ApplicationError = require('../../application-error')

class CardTypeInvalidError extends ApplicationError {
  constructor(message, cardId) {
    super(message)
    this.name = 'CardTypeInvalidError'
    this.cardId = cardId
  }
}

module.exports = CardTypeInvalidError
