const ApplicationError = require('../../application-error')

class CardIdInvalidError extends ApplicationError {
  constructor(message, cardId) {
    super(message)
    this.name = 'CardIdInvalidError'
    this.cardId = cardId
  }
}

module.exports = CardIdInvalidError

