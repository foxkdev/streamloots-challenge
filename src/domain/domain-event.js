const {v4: uuidv4} = require('uuid')
class DomainEvent {
  constructor({name, domain}) {
    if (this.constructor === DomainEvent) {
      throw new TypeError('Abstract class Domain event cannot be instantiated directly')
    }
    this.id = uuidv4
    this.name = name
    this.domain = domain
    this.createdAt = new Date()
  }
}
module.exports = DomainEvent
