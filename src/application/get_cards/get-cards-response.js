module.exports = ({cards}) => {
  return {
    cards: cards.map((card) => {
      return {
        id: card.id,
        name: card.name,
        image: card.image,
        rarity: card.rarity,
        status: card.status,
        type: {
          type: card.cardType.type,
          availableCards: card.cardType.availableCards,
        },
        ownerId: card.ownerId,
        stats: card.stats,
      }
    }),
  }
}
