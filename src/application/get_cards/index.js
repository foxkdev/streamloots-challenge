const getCardsResponse = require('./get-cards-response')
class GetCards {
  constructor({cardRepository, cardsStatsService}) {
    this.cardRepository = cardRepository
    this.cardsStatsService = cardsStatsService
  }
  async get({userId}) {
    const cards = await this.cardRepository.getCards()
    for (const card of cards) {
      if (card.ownerId === userId) {
        card.stats = await this.cardsStatsService.getOwnerStats({cardId: card.id})
      } else {
        card.stats = await this.cardsStatsService.getUserStats({
          cardId: card.id,
          userId,
        })
      }
    }
    return getCardsResponse({cards})
  }
}
module.exports = GetCards
