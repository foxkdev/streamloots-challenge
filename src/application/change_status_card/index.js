const CardIdInvalidError = require('../../domain/card/errors/card-id-invalid-error')
const CardStatusInvalidError = require('../../domain/card/errors/card-status-invalid-error')
const changeStatusCardResponse = require('./change-status-card-response')
const {PUBLISHED, HIDDEN} = require('../../domain/card/card-status')
class ChangeStatusCard {
  constructor({cardRepository, eventBus}) {
    this.cardRepository = cardRepository
    this.eventBus = eventBus
  }
  async publish({cardId, userId}) {
    const card = await this.cardRepository.findByIdAndOwnerId({cardId, ownerId: userId})
    if (card) {
      if (card.status === PUBLISHED) {
        throw new CardStatusInvalidError(`card with id ${card.id} have status published`, cardId)
      }
      card.publish()
      this.cardRepository.update(card)
      this.eventBus.publish(card.getEvents())
      return changeStatusCardResponse({id: card.id, status: card.status})
    }
    throw new CardIdInvalidError(`card with id ${cardId} is invalid to this userId`, cardId)
  }
  async unpublish({cardId, userId}) {
    const card = await this.cardRepository.findByIdAndOwnerId({cardId, ownerId: userId})
    if (card) {
      if (card.status === HIDDEN) {
        throw new CardStatusInvalidError(`card with id ${card.id} have status unpublish`, cardId)
      }
      card.unpublish()
      this.cardRepository.update(card)
      return changeStatusCardResponse({id: card.id, status: card.status})
    }
    throw new CardIdInvalidError(`card with id ${cardId} is invalid to this userId`, cardId)
  }
}
module.exports = ChangeStatusCard
