const updateCardResponse = require('./update-card-response')
const CardIdInvalidError = require('../../domain/card/errors/card-id-invalid-error')
class UpdateCard {
  constructor({cardRepository}) {
    this.cardRepository = cardRepository
  }
  async update({id, name, image, rarity, cardType, ownerId}) {
    const card = await this.cardRepository.findById(id)
    if (card) {
      card.name = name
      card.image = image
      card.rarity = rarity
      card.cardType = cardType || card.cardType
      card.ownerId = ownerId || card.ownerId
      const cardUpdated = await this.cardRepository.update(card)
      return updateCardResponse(cardUpdated)
    } else {
      throw new CardIdInvalidError(`card with id ${id} is invalid`, id)
    }
  }
}
module.exports = UpdateCard
