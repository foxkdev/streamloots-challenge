module.exports = ({id, status, name, image, rarity, cardType, ownerId}) => {
  return {
    id,
    status,
    name,
    image,
    rarity,
    type: {
      type: cardType.type,
      availableCards: cardType.availableCards,
    },
    ownerId,
  }
}
