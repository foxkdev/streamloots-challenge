module.exports = ({id, name, image, rarity, cardType, ownerId}) => {
  return {
    id,
    name,
    image,
    rarity,
    cardType,
    ownerId,
  }
}
