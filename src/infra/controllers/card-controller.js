const express = require('express')
const {header, check} = require('express-validator')
const container = require('../../container')
// eslint-disable-next-line new-cap
const router = express.Router()

const {OK, BAD_REQUEST} = require('../http-status-code')
const {isBodyValid} = require('./middleware/rest-validator')
const {isAuthorized} = require('./middleware/check-authorization')

const getCardsCommandBuilder = require('../../application/get_cards/get-cards-command')
const updateCardCommandBuilder = require('../../application/update_card/update-card-command-builder')
const changeStatusCardCommandBuilder = require('../../application/change_status_card/change-status-card-command-builder')

router.get('/', [
  header('Authorization').notEmpty(),
],
isBodyValid,
isAuthorized,
async (req, res, next) => {
  try {
    const userId = req.userId
    const getCards = container.resolve('getCards')
    const getCardsCommand = getCardsCommandBuilder({userId})
    const getCardsResponse = await getCards.get(getCardsCommand)
    return res.status(OK).json(getCardsResponse)
  } catch (err) {
    console.log(err)
    return res.status(BAD_REQUEST).json(err)
  }
})


router.put('/publish', [
  header('Authorization').notEmpty(),
  check('cards').notEmpty(),
],
isBodyValid,
isAuthorized,
async (req, res, next) => {
  const userId = req.userId
  const cardsIds = req.body.cards
  const errors = []
  const cardsPublishedIds = []

  const changeStatusCard = container.resolve('changeStatusCard')
  for (const cardId of cardsIds) {
    const changeStatusCardCommand = changeStatusCardCommandBuilder({cardId, userId})
    try {
      const changeStatusCardResponse = await changeStatusCard.publish(changeStatusCardCommand)
      cardsPublishedIds.push(changeStatusCardResponse)
    } catch (err) {
      errors.push({message: err.message, cardId: err.cardId})
    }
  }
  return res.status(OK).json({cardsPublishedIds, errors})
}
)

router.put('/unpublish', [
  header('Authorization').notEmpty(),
  check('cards').notEmpty(),
],
isBodyValid,
isAuthorized,
async (req, res, next) => {
  const userId = req.userId
  const cardsIds = req.body.cards
  const errors = []
  const cardsPublishedIds = []
  const changeStatusCard = container.resolve('changeStatusCard')
  for (const cardId of cardsIds) {
    const changeStatusCardCommand = changeStatusCardCommandBuilder({cardId, userId})
    try {
      const changeStatusCardResponse = await changeStatusCard.unpublish(changeStatusCardCommand)
      cardsPublishedIds.push(changeStatusCardResponse)
    } catch (err) {
      errors.push({message: err.message, cardId: err.cardId})
    }
  }
  return res.status(OK).json({cardsPublishedIds, errors})
}
)

router.put('/:cardId', [
  header('Authorization').notEmpty(),
],
isBodyValid,
isAuthorized,
async (req, res, next) => {
  const {cardId} = req.params
  const {name, image, rarity, cardType, ownerId} = req.body
  try {
    const updateCard = container.resolve('updateCard')
    const updateCardCommand = updateCardCommandBuilder({id: cardId, name, image, rarity, cardType, ownerId})
    const updateCardsResponse = await updateCard.update(updateCardCommand)
    return res.status(OK).json(updateCardsResponse)
  } catch (err) {
    return res.status(BAD_REQUEST).json(err)
  }
}
)


module.exports = router

