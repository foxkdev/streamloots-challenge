const jwt = require('jsonwebtoken')
const {NOT_AUTHORIZED} = require('../../http-status-code')
const isAuthorized = (req, res, next) => {
  const {headers} = req
  // eslint-disable-next-line no-unused-vars
  const [_, token] = headers['authorization'].split(' ')
  const tokenDecoded = jwt.decode(token) // change this with validate to validate if jwt is valid
  if (!token || !tokenDecoded) {
    return res.status(NOT_AUTHORIZED).json({message: 'You don not have permissions'})
  }
  req.userId = tokenDecoded.userId
  next()
}
module.exports = {isAuthorized}
