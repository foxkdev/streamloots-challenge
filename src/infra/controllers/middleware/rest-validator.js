const {validationResult} = require('express-validator')
const {BAD_REQUEST} = require('../../http-status-code')

const isBodyValid = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(BAD_REQUEST).json({errors: formatErrors(errors.array())})
  }
  next()
}

const formatErrors = (errors) => {
  return errors.map(({value, param}) => {
    if (value) {
      return {
        message: `Provided value '${param}' has no correct format for field`,
        description: 'invalid params',
      }
    }
    return {
      message: `Field '${param}' cannot be blank`,
      description: 'invalid params',
    }
  })
}

module.exports = {isBodyValid}
