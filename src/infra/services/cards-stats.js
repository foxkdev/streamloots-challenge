// this is service implemention of another service with responsability of have stats of cards
// for me its better have another API with domain "responsability" to have stats of cards based in CardId and userId
// so in this implementation we can have axios requests to get stats
class CardsStats {
  async getOwnerStats({cardId}) {
    // here we can mock to another micro-service to get stats for specific card
    // with before context, the micro-service stats can search in stats with cardID to get global stats
    return {totalObtainedCards: 10, totalUsedCardsByAllUsers: 100}
  }
  async getUserStats({cardId, userId}) {
    // here we can mock to another micro-service to get stats for specific card
    // with before context, the micro-service stats can search in stats with cardId and userID to get specific stats
    return {totalObtainedCards: 10, totalUsedCards: 5}
  }
}
module.exports = CardsStats
