const Events = require('events')
class EventBus {
  constructor() {
    this.localEvents = new Events({captureRejections: true})
  }
  publish(events) {
    events.forEach((event) => {
      this.localEvents.emit(event.name, event)
      this.localEvents.on('error', (err) => console.log(err))
    })
  }
  subscribe(event, callback) {
    this.localEvents.on(event, callback)
  }
}

module.exports = EventBus
