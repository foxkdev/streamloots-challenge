class SubscriberCreatedCard {
  constructor({eventBus}) {
    this.eventBus = eventBus
  }
  subscribe() {
    this.eventBus.subscribe('card-created', (event) => {
      console.log(`EVENT: ${event.name} - Card ID: ${event.domain.id}`)
      this.sendMetricToAnalyticsA(event)
      this.sendMetricToAnalyticsB(event)
    })
  }
  sendMetricToAnalyticsA(event) {
    // here we send metrics to analytics Platform A with API or with npm package
    console.log('event sended to analytics platform A')
    return true
  }
  sendMetricToAnalyticsB(event) {
    // here we send metrics to analytics Platform B with API or with npm package
    console.log('event sended to analytics platform B')
    return true
  }
}
module.exports = SubscriberCreatedCard

