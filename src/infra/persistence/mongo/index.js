const mongoose = require('mongoose')
const fs = require('fs')
module.exports = ({config: {mongo: {uri: mongoConnectionUri}}}) => {
  mongoose.connect(mongoConnectionUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  const models = {}
  fs.readdirSync(__dirname + '/models').forEach((file) => {
    const Factory = require(`./models/${file}`)
    const model = new Factory({mongoose})
    models[model.name] = model.model
  })
  return {
    mongoose,
    models,
  }
}
