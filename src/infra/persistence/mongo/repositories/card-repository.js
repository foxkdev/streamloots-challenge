const Card = require('../../../../domain/card/card')
const Repository = require('./repository')
const CardIdInvalidError = require('../../../../domain/card/errors/card-id-invalid-error')
class CardRepository extends Repository {
  constructor({db}) {
    super({db})
    this.model= this.db.models.Card
  }
  async getCards() {
    const cards = await this.model.find()
    return cards.map((card) => {
      return this.toDomain(card)
    })
  }
  async findByIdAndOwnerId({cardId, ownerId}) {
    const card = await this.model.findOne({_id: cardId, ownerId})
    return card ? this.toDomain(card) : null
  }
  async update(domain) {
    const cardDocument = this.toDocument(domain)
    try {
      const card = await this.model.findOneAndUpdate({_id: cardDocument.id}, cardDocument, {new: true})
      return card ? this.toDomain(card) : null
    } catch (err) {
      throw new CardIdInvalidError(`Card with id ${cardDocument.id} is not valid to update`, cardDocument.id)
    }
  }
  toDocument(domain) {
    return {
      id: domain.id,
      name: domain.name,
      image: domain.image,
      rarity: domain.rarity,
      status: domain.status,
      type: domain.cardType?.type,
      availableCards: domain.cardType?.availableCards,
      ownerId: domain.ownerId,
      createdAt: domain.createdAt,
      updatedAt: domain.updatedAt,
    }
  }
  toDomain(document) {
    const {id, name, image, rarity, status, type, availableCards, ownerId, createdAt, updatedAt} = document
    const card = Card.build({id, name, image, rarity, status, cardType: {type, availableCards}, ownerId, createdAt, updatedAt})
    return card
  }
}
module.exports = CardRepository
