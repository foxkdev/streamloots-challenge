class Card {
  constructor({mongoose}) {
    const CardSchema = new mongoose.Schema({
      name: String,
      image: String,
      rarity: String,
      status: String,
      type: String,
      availableCards: Number,
      ownerId: String,
      createdAt: Date,
      updatedAt: Date,
    })
    this.model = mongoose.model('Card', CardSchema)
    this.name = 'Card'
  }
}

module.exports = Card
